from django.test import TestCase, Client
from django.http import HttpRequest
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import os

from .models import StatusModel
from .views import indexStatus

# Faker package to generate dummy data.
from faker import Faker

# Create your tests here.
class StatusUnitTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_landing_page_have_form(self):
        request = HttpRequest()
        response = indexStatus(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)

    def test_can_create_StatusModel(self):
        StatusModel.objects.create(nama='Penulis', pesan='Pesan yang ditulis')
        counter = StatusModel.objects.all().count()
        self.assertEqual(counter, 1)


    def test_can_show_statusModel_on_landing_page(self):
        faker = Faker()
        for _ in range(5):
            status = StatusModel.objects.create(nama=faker.name(), pesan=faker.text())

        response = self.client.get('/')

        # Use assertContains (from response) , use assertIn (must precedded with content.decode).
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, status.nama)
        self.assertContains(response, status.pesan)


    def test_submit_page_url_is_exist(self):
        response = self.client.get('/addstatus/')
        self.assertEqual(response.status_code, 302)

    def test_submit_page_contains_post_data(self):
        faker = Faker()
        name = faker.name()
        status = faker.text()
        response = self.client.post('/addstatus/', data={'nama': name, 'status': status})
        self.assertContains(response, name)
        self.assertContains(response, status)

    def test_confirm_page_success_model(self):
        faker = Faker()
        name = faker.name()
        status = faker.text()
        response = self.client.post('/confirmstatus/', data={
            'nama': name,
            'status': status,
            'confirm': 'yes'
        })
        self.assertEqual(response.status_code, 200)

        # Checking data in models.
        data = StatusModel.objects.get(nama=name)
        self.assertEqual(data.nama, name)
        self.assertEqual(data.pesan, status)

        # Checking new data in landing page.
        response = self.client.get('/')
        self.assertContains(response, name)
        self.assertContains(response, status)

    def test_check_no_name_and_no_status(self):
        response = self.client.post('/addstatus/', data={
            'nama': '',
            'status': ''
        })

        # Status cannot be empty.
        self.assertEqual(response.status_code, 200)

    def test_check_model_return_str(self):
        faker = Faker()

        name = faker.name()
        status = faker.text()
        data = StatusModel.objects.create(nama=name, pesan=status)

        self.assertEqual(str(data), name + ' - ' + status)

    def test_check_confirm_anonymous(self):
        faker = Faker()

        status = faker.text()
        response = self.client.post('/addstatus/', {
            'nama': '',
            'status': status
        })

        self.assertContains(response, 'Anonim')

    def test_check_redirect_confirm_status_refreshed(self):
        response = self.client.get('/confirmstatus/')

        self.assertEqual(response.status_code, 302)


class StatusFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)

        super(StatusFunctionalTest, self).setUp()


    def tearDown(self):
        self.browser.quit()
        super(StatusFunctionalTest, self).tearDown()

    def test_input_form(self):
        # Generate dummy data with Faker.
        faker = Faker()
        nama = faker.name()
        pesan = faker.text()

        # Opening the link we want to test.
        self.browser.get('http://localhost:8000/')

        # Find the form element.
        nama_field = self.browser.find_element_by_id('inputName')
        pesan_field = self.browser.find_element_by_id('inputStatus')
        submit_btn = self.browser.find_element_by_class_name('status-submit-button')

        # Fill the form with data.
        nama_field.send_keys(nama)
        pesan_field.send_keys(pesan)
        time.sleep(5)

        # Submitting the form.
        submit_btn.click()
        time.sleep(5)

        # Confirming the form. First, find the element first, in this new html page
        confirm_btn = self.browser.find_element_by_class_name('confirm-button')
        confirm_btn.click()
        time.sleep(5)

        # Checking result on landing page and on model
        self.browser.get('http://localhost:8000/')
        self.assertIn(nama, self.browser.page_source)
        self.assertIn(pesan, self.browser.page_source)

        time.sleep(10)