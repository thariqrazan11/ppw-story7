from django.urls import path
from . import views

urlpatterns = [
    path('', views.indexStatus),
    path('addstatus/', views.addStatus),
    path('confirmstatus/', views.confirmStatus),
]